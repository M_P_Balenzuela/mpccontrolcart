/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_pm_delta_info.c
 *
 * Code generation for function '_coder_pm_delta_info'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "pm_delta.h"
#include "_coder_pm_delta_info.h"

/* Function Definitions */
mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 4);
  emlrtSetField(xEntryPoints, 0, "Name", mxCreateString("pm_delta"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", mxCreateDoubleScalar(4.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", mxCreateDoubleScalar(4.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", mxCreateString("9.2.0.556344 (R2017a)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[25] = {
    "789ced5dcd6f1bb9151fa7499a14d8d6e8271645bb4e50a41f012c59966d79b1c15a5f96645bb63e6dd9412a8f462389d67c6966f4615fea5e8a1efb07f49063"
    "7bdbde7a0960143deca1d72ed0a2871e8bf6d253cf9534435b2388ab8987a23c63120868f2cdf0c779797c8f7c8fa49885547a816198af3346fac97f8cfc03b3",
    "bc68e60f186b1aa72f98f98fc7ca303d621e5ade83f4df9a39274b3adfd38d8200247ebf2d5678b55f905891bf6ea62a8b406225bd70aef08cca6bb2d0e1ab43"
    "4a0d087c0188fc9e3c5248827e41dc1e215d1706a4c1dfd106cf35f36d91511bda4d7785d10233c29fb788ef7f68933faf10fc5934eb20fd75fc4df4635f51e3",
    "55cdc7adaeac07026b1bbe982a2b15b9e78bb2aabe94ce447d5da037961abca60156f22962b9ca0b3abb2cdae8ef639bfd7d82e8ef5333dfb2d426cde2b699c7"
    "cdfc13337f65e497b03e6196135bb0bfa788fed8e5efe3b13273fddce3619da8c8dd815891c2abcaed8ac0dfe0fdce215e128967d441faebd45e6928421955ae",
    "abacb8349075cd970e17f6c2115f2ee05fd9607dba2c0b0379e245c127808a4f647581adf86445f3197c5a16a7f1c9ae1c8de7303dbd96b04be5a30fff1a2687"
    "67a4fb82d743b46757eebe8bc0837207e9a58bf5625a8f0356dc6c7537e4ec2ac7e542d19b7e64a6e04ceb078328936adfade35741f4cbaebc3d182bc304edc0",
    "a2a5d6bf65e4816bbd7e8968df2edf3e42e043be413a2757fb9f0dfa930955628565a045da40d053527f3ac1ab809b9bdeffa343bc1212cfa883f4f7969bc1bf"
    "9743aef95e42b6f9c6d946d40efce2cf7fa776c0ed76e0a0d38a866bc94ea8900d9e7371510a6c9f08496a07e66d074e11fdc22b6f9f629bcfa3d61f8b7dcaa0",
    "0e68fd8f55416f5e7afd3387787b483ca30ed29dc8012ff4fff0414e0d978724f5cf9fde517dee7a7dded1b9b40ae4a2b816b8909a877b9bdd7429b94df5f9bd"
    "d0e797116cfafc11026fb14f19d499ee19d7fa6712483ca30ed29dca81290604f5cee5a2f617aac7efaa1eff0e020fca1da41f17fd67fe344805d4547de73811",
    "da0cc54a470cd5e3f31ebf64fd33cfb78cfc05f5cf98f5d43f33398789fa67c8e051ff0c9ef69daecb33636566e4b985113a263bf0ac26e8c33fc9ceebd35bb8"
    "e4ee7b083cc82f481fd3ff1ac70aac1aafcf4defdf593999a8f721bb88ea7b5ef807d5f76ed7f7fefdee865800e2c92ec855e3bb312e5fab9d47a8bebf3fe398",
    "b1e87bd47ac32ebf3e182b3323cf2d8cd081261953547db0636b7e7e1da7f27180c433ea20fd56f3811ae8f15545ee8b87cfc22fa2fe1d46a4fb6fdcafe713ab"
    "cdf37cab0e846821daccb522f542b3188f513d7f5ff53c0a0f5ffcb506facb99865bf5fa0e12cfa88374e77adde093117d25aa87a85e9f211e29bf7da7c5c795",
    "0ea76d96baabb29cd1f22babc7278c77f4babbfcaf377afd590da89a5e0344e5efea149bdffe87083cc83748efb3a1dcfffc724d56055956ca7287576b82dc2d"
    "7383f313cee7f5e309d51f9820ded52df1605ba763e5713c4877b4ffc610a32fe11f517f0ef387475f507b30233c52f37c6d37de8bc72e2e323bbd40301df047",
    "b3e1157fdc3bf6e05f88f7edf2f15788f6211f217dd6e3fad9973f506ef08262e73c125e79c577deeae15899b97eeee1f5fa60509e739c014855be9792746cfb"
    "04b6a7f403d271ac1b066b0682f27149e3bbeeb70ff97305886baddcd10aa85483a9936c26bdd2f1901f888e5f6bb2ca9bdf337efe69f6856bb083eda2d4cf8f",
    "c861a2fe203278d4cf8fa77d3affb77e175e794d11bb6fa1ffe922db73af3d98697cc07a3e6bc8a939c407646a0fdc6f0f84defa09ef3f3bf01741c45fe85eac"
    "b04121e3a1fd9cd41e58bf0b6fbc98ac3d0012b507f6ec0190a83df0161e297bb0964a3795e63edfd393e97c3d93d8cd24fc8287f67bd278f1e41c268bfcfdf3",
    "149b7e7f5f7fbf533cd2e7773dec17a47efd19e251bf3e9ef69dcecf32636566e4b905c623e7b6989bfd9dbf46b467975fcf1178905f903e164f65154538cf0f"
    "adda765be274204bc673b8e20edf98d22f48af99e8e5062b55fb8600d7bea0d753f0211dcffc60023bcd893f41b9a2e77a678847ca3ec445500807cf235541e1",
    "77d39b75e55802110fcdfbaf10efd3716d4d1639dcca61db2ffa7d041ee41fa44f38e7cbf794b020c8dc903eaff581d37563018967d4413abe7523641be97bdb"
    "e83d0fb3c423650f5295706a655d6976e287ad237f33e20f66c18e87ec011dcf93739850fb82e6b46e1865c0be1c35ce0e0cd2bcecc195433cb2f38909ec233e",
    "9fa076619678a4ec821ed83d2fb5eaa5c346bdb9914be8cd9d4c2feea178f115e27dbb7c3c41b40ff908e9b8fc484684a05c1364562fc34b3019c272787548ef"
    "0372883733ffe39d395f4eef039a251ebd0f084ffb5788f7edf2f10da27dc84748c71d47285765910552995755d9ce3dc878f57f165b9c78da3dcf4093d841c8",
    "c0ad7a3e85c433ea201dc7bdfd7d4ec16d4024e7f9345e3c433c52f746b4c2ed0331785e0afa736aa01d5de1373be23ee31d3d4fc7f1e4fe5be5ee47d8fcfff4"
    "9e67149e5147ef79be2d9e91ee0b1ebde7194ffb5788f7e93cdf9ae635cfaf09b2adefbbdff3fc5a5bf20d39358ff9c12f7ff082ea79b7ebf9506be3a0d01262",
    "6bcd48a3904de4f5562cc679e8f7b9e8389edc7fabdc2dd179be594fe7f9937398e83c9f0c1e9de7e3699feaffc9fd9f959fc7c13e4f565d86b3fef9dd0bea54"
    "ff17a7e0413a1efdffd2e0db88ec10d4ff67f4bcaffbf57fa5b19e2949825ecdcb62bc2d25572bf1c84e82eafffbaaff7f8368cf2ebf7e8ac083fc82f4e9e7c2",
    "5252466039de7cfeae9c0ffbdc217e650a3ea4cfec1c89c9d639dc0ff18efe9ebbfbed45b21b8be67aa18b55994df23bdb7a883ba8453c642fe8f89efc5df6e4"
    "71d973eb8879f991e83a62567846ba2f78741d81a77da7eb88cc589919796e81f1c8fd1297fbd8ce017c1b8107f905e963fa1f86c5b1e97dd2f7c4a59178461d",
    "a4e3d1fb437691d6f7ef823cd5f76ed7f7ed60bc580ab4777bda7e438ba4cff2ebda1aefa173006f11efdbe5e32b44fb908f90fe7a38888b1aaf6a3e6e75653d"
    "1058dbf0c55459198cdb28abea4be94cd4d7057a63a9c16b1a60259f2296abbca0b396718bf2dbd89533d4fcf6a999fb2db5c92d23df36f3b8997f62e6afcc1c",
    "dfefc57c05d1bfc53e6550a70d2e0765dc7b7f5c028967d441ba733fa336bc1c94ee13f2061e297dcfb2c9c849b7990487e9685b8844a4d84666df43f7c7d1f1"
    "6b4db38a0f7f88c0837c82f4c97e1da8e5dd1b1fce4fc18374bc7e1d0d90bf3fea92f9f9efa9de77bbdef7af25d743d1ee51c01fe97605bf220463f50b0fcdf3",
    "a9deb7261a179edc3f1a17b6f69bc685dd8147e3c278daa7e37bf277d993477c71e1bbb27eb8ad9f88ae1f26e730d1f503193cba7ec0d3fe5bc4fb77354ef019"
    "a23f76e5ec6b88fec23841c6526b334e7009eb1366d9cc995db3bc472c8ec0c9dab04ce308d3d6a37d4ed1388267f048d903a5d03bdb38d033eb1b076c217f56",
    "5013e79ce4217b40c7af35ddcd3802d4f2348ef07eeb00436646bfe314d14f8c72f3bf37741de07abd1f52f73436cba58f8f2ac7a54a24b21eacd5bdb45f88ea"
    "7d6ba27184c9fda371046bbf691cc11d78348e80a77d3abe277f973d79bc2b71047ceb87dbfa89e8fa61720e135d3f90c1a3eb073cedbf45bc4fe30883f4be71",
    "843d331f8b27608c233c192b33d7cf3d19d6896a157440959f977d40fdffd8c5db41e2197590eef4bc22e4d3b2b9f022a8c7fef6ada52fbc6c175af33c574dea"
    "770a7ac72727f17a18a8ad60478caf158ec3d93396f18e5da0e37872ffad72f7df4f71e9d9af22f016fb94419df999739bf73b950712f7590de4018a83b94c24",
    "b96f48fb7786cef7ddaed743dd6cad580aae6f243b426e33160aedc9458ea17afdae8c63949fdcaedc3d40f41fceff172db5cfb78cfc0536ff0fbda71a8567d4"
    "d17baa6f8b67a4fb8247efa9c6d3bedbedc129a27f78e5ee67d8f4ff12020ff20bd227ff0e7d54161556077d8d3c2ffd7fe510ef188967d4413ad6df9fbf61db",
    "3ce4a749ef9973bf1da8f48ef77335b679984defeeefe4783defcf1f503b70cfecc0c75bb8e4ee9b083cc82f481fb303fdaf37eaddea07da45e21975908e47ff"
    "f7d945fefc5893fa81dcafefd345dd7f9c3a0a4b523107325a76bfae155a71f7ebfbff03114bd8f0",
    "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 60224U, &nameCaptureInfo);
  return nameCaptureInfo;
}

/* End of code generation (_coder_pm_delta_info.c) */
