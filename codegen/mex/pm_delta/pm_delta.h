/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * pm_delta.h
 *
 * Code generation for function 'pm_delta'
 *
 */

#ifndef PM_DELTA_H
#define PM_DELTA_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "pm_delta_types.h"

/* Function Declarations */
extern void pm_delta(const emlrtStack *sp, const real_T xk[4], real_T uk, real_T
                     Delta, const struct0_T *param, real_T h[4], real_T dhdxT[16],
                     real_T dhduT[4], real_T *Q);

#endif

/* End of code generation (pm_delta.h) */
