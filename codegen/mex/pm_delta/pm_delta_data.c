/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * pm_delta_data.c
 *
 * Code generation for function 'pm_delta_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "pm_delta.h"
#include "pm_delta_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131450U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "pm_delta",                          /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

emlrtRSInfo emlrtRSI = { 28,           /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

emlrtRSInfo b_emlrtRSI = { 29,         /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

emlrtRSInfo c_emlrtRSI = { 42,         /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

emlrtRSInfo d_emlrtRSI = { 43,         /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

emlrtRSInfo e_emlrtRSI = { 44,         /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

emlrtRSInfo f_emlrtRSI = { 46,         /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

emlrtRSInfo g_emlrtRSI = { 47,         /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

emlrtRSInfo h_emlrtRSI = { 48,         /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

emlrtRSInfo i_emlrtRSI = { 37,         /* lineNo */
  "mpower",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\ops\\mpower.m"/* pathName */
};

emlrtRSInfo j_emlrtRSI = { 49,         /* lineNo */
  "power",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\ops\\power.m"/* pathName */
};

/* End of code generation (pm_delta_data.c) */
