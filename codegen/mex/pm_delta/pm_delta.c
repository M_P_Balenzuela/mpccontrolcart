/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * pm_delta.c
 *
 * Code generation for function 'pm_delta'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "pm_delta.h"
#include "mpower.h"

/* Function Definitions */
void pm_delta(const emlrtStack *sp, const real_T xk[4], real_T uk, real_T Delta,
              const struct0_T *param, real_T h[4], real_T dhdxT[16], real_T
              dhduT[4], real_T *Q)
{
  real_T costheta2;
  real_T B;
  real_T b_B;
  real_T A;
  real_T c_B;
  real_T b_A;
  real_T y;
  real_T c_A;
  real_T b_y;
  (void)sp;

  /*  Unpack States */
  /*  Unpack Inputs */
  /*  Unpack parameter structure */
  /*  Euler prediction */
  /*  Euler prediction */
  /*  ToDo: Q uncertainty */
  *Q = 0.0;
  h[0] = xk[0] + Delta * xk[2];
  h[1] = xk[1] + Delta * xk[3];
  costheta2 = (uk - param->b * xk[2]) + mpower(xk[3]) * param->l * param->m *
    muDoubleScalarSin(xk[1]);
  B = (param->M + param->m) - param->m * mpower(muDoubleScalarCos(xk[1]));
  b_B = (param->M * param->l + param->l * param->m) - param->l * param->m *
    mpower(muDoubleScalarCos(xk[1]));
  h[2] = xk[2] + Delta * (costheta2 / B + muDoubleScalarCos(xk[1]) * (param->c *
    xk[3] - param->g * param->l * param->m * muDoubleScalarSin(xk[1])) / b_B);
  B = (mpower(param->l) * mpower(param->m) - mpower(param->l) * mpower(param->m)
       * mpower(muDoubleScalarCos(xk[1]))) + param->M * mpower(param->l) *
    param->m;
  costheta2 = muDoubleScalarCos(xk[1]) * ((uk - param->b * xk[2]) + mpower(xk[3])
    * param->l * param->m * muDoubleScalarSin(xk[1]));
  b_B = (param->M * param->l + param->l * param->m) - param->l * param->m *
    mpower(muDoubleScalarCos(xk[1]));
  h[3] = xk[3] - Delta * ((param->c * xk[3] - param->g * param->l * param->m *
    muDoubleScalarSin(xk[1])) * (param->M + param->m) / B + costheta2 / b_B);

  /*  State derivities w.r.t. previous states */
  dhdxT[0] = 1.0;
  dhdxT[4] = 0.0;
  dhdxT[8] = Delta;
  dhdxT[12] = 0.0;
  dhdxT[1] = 0.0;
  dhdxT[5] = 1.0;
  dhdxT[9] = 0.0;
  dhdxT[13] = Delta;
  dhdxT[2] = 0.0;
  B = (param->M * param->l + param->l * param->m) - param->l * param->m * mpower
    (muDoubleScalarCos(xk[1]));
  costheta2 = mpower(xk[3]) * param->l * param->m * muDoubleScalarCos(xk[1]);
  b_B = (param->M + param->m) - param->m * mpower(muDoubleScalarCos(xk[1]));
  A = param->g * param->l * param->m * mpower(muDoubleScalarCos(xk[1]));
  c_B = (param->M * param->l + param->l * param->m) - param->l * param->m *
    mpower(muDoubleScalarCos(xk[1]));
  b_A = 2.0 * param->m * muDoubleScalarCos(xk[1]) * muDoubleScalarSin(xk[1]) *
    ((uk - param->b * xk[2]) + mpower(xk[3]) * param->l * param->m *
     muDoubleScalarSin(xk[1]));
  y = mpower((param->M + param->m) - param->m * mpower(muDoubleScalarCos(xk[1])));
  c_A = 2.0 * param->l * param->m * mpower(muDoubleScalarCos(xk[1])) *
    muDoubleScalarSin(xk[1]) * (param->c * xk[3] - param->g * param->l *
    param->m * muDoubleScalarSin(xk[1]));
  b_y = mpower((param->M * param->l + param->l * param->m) - param->l * param->m
               * mpower(muDoubleScalarCos(xk[1])));
  dhdxT[6] = -Delta * ((((muDoubleScalarSin(xk[1]) * (param->c * xk[3] -
    param->g * param->l * param->m * muDoubleScalarSin(xk[1])) / B - costheta2 /
    b_B) + A / c_B) + b_A / y) + c_A / b_y);
  B = (param->M + param->m) - param->m * mpower(muDoubleScalarCos(xk[1]));
  dhdxT[10] = 1.0 - Delta * param->b / B;
  B = (param->M * param->l + param->l * param->m) - param->l * param->m * mpower
    (muDoubleScalarCos(xk[1]));
  b_B = (param->M + param->m) - param->m * mpower(muDoubleScalarCos(xk[1]));
  dhdxT[14] = Delta * (param->c * muDoubleScalarCos(xk[1]) / B + 2.0 * xk[3] *
                       param->l * param->m * muDoubleScalarSin(xk[1]) / b_B);
  dhdxT[3] = 0.0;
  costheta2 = muDoubleScalarSin(xk[1]) * ((uk - param->b * xk[2]) + mpower(xk[3])
    * param->l * param->m * muDoubleScalarSin(xk[1]));
  B = (param->M * param->l + param->l * param->m) - param->l * param->m * mpower
    (muDoubleScalarCos(xk[1]));
  A = mpower(xk[3]) * param->l * param->m * mpower(muDoubleScalarCos(xk[1]));
  b_B = (param->M * param->l + param->l * param->m) - param->l * param->m *
    mpower(muDoubleScalarCos(xk[1]));
  c_B = (mpower(param->l) * mpower(param->m) - mpower(param->l) * mpower
         (param->m) * mpower(muDoubleScalarCos(xk[1]))) + param->M * mpower
    (param->l) * param->m;
  b_A = 2.0 * param->l * param->m * mpower(muDoubleScalarCos(xk[1])) *
    muDoubleScalarSin(xk[1]) * ((uk - param->b * xk[2]) + mpower(xk[3]) *
    param->l * param->m * muDoubleScalarSin(xk[1]));
  y = mpower((param->M * param->l + param->l * param->m) - param->l * param->m *
             mpower(muDoubleScalarCos(xk[1])));
  c_A = 2.0 * mpower(param->l) * mpower(param->m) * muDoubleScalarCos(xk[1]) *
    muDoubleScalarSin(xk[1]) * (param->c * xk[3] - param->g * param->l *
    param->m * muDoubleScalarSin(xk[1])) * (param->M + param->m);
  b_y = mpower((mpower(param->l) * mpower(param->m) - mpower(param->l) * mpower
                (param->m) * mpower(muDoubleScalarCos(xk[1]))) + param->M *
               mpower(param->l) * param->m);
  dhdxT[7] = Delta * ((((costheta2 / B - A / b_B) + param->g * param->l *
                        param->m * muDoubleScalarCos(xk[1]) * (param->M +
    param->m) / c_B) + b_A / y) + c_A / b_y);
  B = (param->M * param->l + param->l * param->m) - param->l * param->m * mpower
    (muDoubleScalarCos(xk[1]));
  dhdxT[11] = Delta * param->b * muDoubleScalarCos(xk[1]) / B;
  B = (mpower(param->l) * mpower(param->m) - mpower(param->l) * mpower(param->m)
       * mpower(muDoubleScalarCos(xk[1]))) + param->M * mpower(param->l) *
    param->m;
  b_B = (param->M * param->l + param->l * param->m) - param->l * param->m *
    mpower(muDoubleScalarCos(xk[1]));
  dhdxT[15] = 1.0 - Delta * (param->c * (param->M + param->m) / B + 2.0 * xk[3] *
    param->l * param->m * muDoubleScalarCos(xk[1]) * muDoubleScalarSin(xk[1]) /
    b_B);

  /*  State derivities w.r.t. input */
  dhduT[0] = 0.0;
  dhduT[1] = 0.0;
  costheta2 = muDoubleScalarCos(xk[1]);
  costheta2 *= costheta2;
  dhduT[2] = Delta / ((param->M + param->m) - param->m * costheta2);
  dhduT[3] = -(Delta * muDoubleScalarCos(xk[1])) / ((param->M * param->l +
    param->l * param->m) - param->l * param->m * costheta2);
}

/* End of code generation (pm_delta.c) */
