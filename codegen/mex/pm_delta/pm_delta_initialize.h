/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * pm_delta_initialize.h
 *
 * Code generation for function 'pm_delta_initialize'
 *
 */

#ifndef PM_DELTA_INITIALIZE_H
#define PM_DELTA_INITIALIZE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "pm_delta_types.h"

/* Function Declarations */
extern void pm_delta_initialize(void);

#endif

/* End of code generation (pm_delta_initialize.h) */
