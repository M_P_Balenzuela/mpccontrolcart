/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * pm_delta_types.h
 *
 * Code generation for function 'pm_delta'
 *
 */

#ifndef PM_DELTA_TYPES_H
#define PM_DELTA_TYPES_H

/* Include files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
  real_T m;
  real_T M;
  real_T l;
  real_T b;
  real_T c;
  real_T g;
} struct0_T;

#endif                                 /*typedef_struct0_T*/
#endif

/* End of code generation (pm_delta_types.h) */
