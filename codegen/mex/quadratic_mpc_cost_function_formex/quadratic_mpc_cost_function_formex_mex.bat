@echo off
set MATLAB=C:\PROGRA~1\MATLAB\R2017a
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\R2017a\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=quadratic_mpc_cost_function_formex_mex
set MEX_NAME=quadratic_mpc_cost_function_formex_mex
set MEX_EXT=.mexw64
call setEnv.bat
echo # Make settings for quadratic_mpc_cost_function_formex > quadratic_mpc_cost_function_formex_mex.mki
echo COMPILER=%COMPILER%>> quadratic_mpc_cost_function_formex_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> quadratic_mpc_cost_function_formex_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> quadratic_mpc_cost_function_formex_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> quadratic_mpc_cost_function_formex_mex.mki
echo LINKER=%LINKER%>> quadratic_mpc_cost_function_formex_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> quadratic_mpc_cost_function_formex_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> quadratic_mpc_cost_function_formex_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> quadratic_mpc_cost_function_formex_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> quadratic_mpc_cost_function_formex_mex.mki
echo BORLAND=%BORLAND%>> quadratic_mpc_cost_function_formex_mex.mki
echo OMPFLAGS= >> quadratic_mpc_cost_function_formex_mex.mki
echo OMPLINKFLAGS= >> quadratic_mpc_cost_function_formex_mex.mki
echo EMC_COMPILER=mingw64>> quadratic_mpc_cost_function_formex_mex.mki
echo EMC_CONFIG=optim>> quadratic_mpc_cost_function_formex_mex.mki
"C:\Program Files\MATLAB\R2017a\bin\win64\gmake" -B -f quadratic_mpc_cost_function_formex_mex.mk
