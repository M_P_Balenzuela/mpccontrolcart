/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_pm_step_formex_api.h
 *
 * Code generation for function '_coder_pm_step_formex_api'
 *
 */

#ifndef _CODER_PM_STEP_FORMEX_API_H
#define _CODER_PM_STEP_FORMEX_API_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "pm_step_formex_types.h"

/* Function Declarations */
extern void pm_step_formex_api(const mxArray *prhs[5], const mxArray *plhs[4]);

#endif

/* End of code generation (_coder_pm_step_formex_api.h) */
