/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_pm_step_formex_api.c
 *
 * Code generation for function '_coder_pm_step_formex_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "pm_step_formex.h"
#include "_coder_pm_step_formex_api.h"
#include "pm_step_formex_data.h"

/* Function Declarations */
static real_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[4];
static const mxArray *b_emlrt_marshallOut(const real_T u[16]);
static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *uk, const
  char_T *identifier);
static const mxArray *c_emlrt_marshallOut(const real_T u[4]);
static real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static const mxArray *d_emlrt_marshallOut(const real_T u);
static struct0_T e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *param,
  const char_T *identifier);
static real_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *xk, const
  char_T *identifier))[4];
static void emlrt_marshallOut(const real_T u[4], const mxArray *y);
static struct0_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static real_T (*g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[4];
static real_T h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);

/* Function Definitions */
static real_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[4]
{
  real_T (*y)[4];
  y = g_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static const mxArray *b_emlrt_marshallOut(const real_T u[16])
{
  const mxArray *y;
  const mxArray *m0;
  static const int32_T iv1[2] = { 0, 0 };

  static const int32_T iv2[2] = { 4, 4 };

  y = NULL;
  m0 = emlrtCreateNumericArray(2, iv1, mxDOUBLE_CLASS, mxREAL);
  mxSetData((mxArray *)m0, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m0, iv2, 2);
  emlrtAssign(&y, m0);
  return y;
}

static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *uk, const
  char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = d_emlrt_marshallIn(sp, emlrtAlias(uk), &thisId);
  emlrtDestroyArray(&uk);
  return y;
}

static const mxArray *c_emlrt_marshallOut(const real_T u[4])
{
  const mxArray *y;
  const mxArray *m1;
  static const int32_T iv3[1] = { 0 };

  static const int32_T iv4[1] = { 4 };

  y = NULL;
  m1 = emlrtCreateNumericArray(1, iv3, mxDOUBLE_CLASS, mxREAL);
  mxSetData((mxArray *)m1, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m1, iv4, 1);
  emlrtAssign(&y, m1);
  return y;
}

static real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = h_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static const mxArray *d_emlrt_marshallOut(const real_T u)
{
  const mxArray *y;
  const mxArray *m2;
  y = NULL;
  m2 = emlrtCreateDoubleScalar(u);
  emlrtAssign(&y, m2);
  return y;
}

static struct0_T e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *param,
  const char_T *identifier)
{
  struct0_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = f_emlrt_marshallIn(sp, emlrtAlias(param), &thisId);
  emlrtDestroyArray(&param);
  return y;
}

static real_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *xk, const
  char_T *identifier))[4]
{
  real_T (*y)[4];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = b_emlrt_marshallIn(sp, emlrtAlias(xk), &thisId);
  emlrtDestroyArray(&xk);
  return y;
}
  static void emlrt_marshallOut(const real_T u[4], const mxArray *y)
{
  static const int32_T iv0[1] = { 4 };

  mxSetData((mxArray *)y, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)y, iv0, 1);
}

static struct0_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  struct0_T y;
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[6] = { "m", "M", "l", "b", "c", "g" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 6, fieldNames, 0U, &dims);
  thisId.fIdentifier = "m";
  y.m = d_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0, "m")),
    &thisId);
  thisId.fIdentifier = "M";
  y.M = d_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0, "M")),
    &thisId);
  thisId.fIdentifier = "l";
  y.l = d_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0, "l")),
    &thisId);
  thisId.fIdentifier = "b";
  y.b = d_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0, "b")),
    &thisId);
  thisId.fIdentifier = "c";
  y.c = d_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0, "c")),
    &thisId);
  thisId.fIdentifier = "g";
  y.g = d_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0, "g")),
    &thisId);
  emlrtDestroyArray(&u);
  return y;
}

static real_T (*g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[4]
{
  real_T (*ret)[4];
  static const int32_T dims[1] = { 4 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  ret = (real_T (*)[4])mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  static real_T h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

void pm_step_formex_api(const mxArray *prhs[5], const mxArray *plhs[4])
{
  real_T (*dxkp1_dxk)[16];
  real_T (*dxkp1_duk)[4];
  real_T (*xk)[4];
  real_T uk;
  real_T T;
  real_T M;
  struct0_T param;
  real_T Qkp1;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  dxkp1_dxk = (real_T (*)[16])mxMalloc(sizeof(real_T [16]));
  dxkp1_duk = (real_T (*)[4])mxMalloc(sizeof(real_T [4]));
  prhs[0] = emlrtProtectR2012b(prhs[0], 0, true, -1);

  /* Marshall function inputs */
  xk = emlrt_marshallIn(&st, emlrtAlias(prhs[0]), "xk");
  uk = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "uk");
  T = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "T");
  M = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "M");
  param = e_emlrt_marshallIn(&st, emlrtAliasP(prhs[4]), "param");

  /* Invoke the target function */
  pm_step_formex(&st, *xk, uk, T, M, &param, *dxkp1_dxk, *dxkp1_duk, &Qkp1);

  /* Marshall function outputs */
  emlrt_marshallOut(*xk, prhs[0]);
  plhs[0] = prhs[0];
  plhs[1] = b_emlrt_marshallOut(*dxkp1_dxk);
  plhs[2] = c_emlrt_marshallOut(*dxkp1_duk);
  plhs[3] = d_emlrt_marshallOut(Qkp1);
}

/* End of code generation (_coder_pm_step_formex_api.c) */
