@echo off
set MATLAB=C:\PROGRA~1\MATLAB\R2017a
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\R2017a\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=pm_step_formex_mex
set MEX_NAME=pm_step_formex_mex
set MEX_EXT=.mexw64
call setEnv.bat
echo # Make settings for pm_step_formex > pm_step_formex_mex.mki
echo COMPILER=%COMPILER%>> pm_step_formex_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> pm_step_formex_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> pm_step_formex_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> pm_step_formex_mex.mki
echo LINKER=%LINKER%>> pm_step_formex_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> pm_step_formex_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> pm_step_formex_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> pm_step_formex_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> pm_step_formex_mex.mki
echo BORLAND=%BORLAND%>> pm_step_formex_mex.mki
echo OMPFLAGS= >> pm_step_formex_mex.mki
echo OMPLINKFLAGS= >> pm_step_formex_mex.mki
echo EMC_COMPILER=mingw64>> pm_step_formex_mex.mki
echo EMC_CONFIG=optim>> pm_step_formex_mex.mki
"C:\Program Files\MATLAB\R2017a\bin\win64\gmake" -B -f pm_step_formex_mex.mk
