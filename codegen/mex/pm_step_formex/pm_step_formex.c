/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * pm_step_formex.c
 *
 * Code generation for function 'pm_step_formex'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "pm_step_formex.h"
#include "mpower.h"
#include "pm_step_formex_data.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 8,     /* lineNo */
  "pm_step_formex",                    /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_step_formex.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 16,  /* lineNo */
  "pm_step_formex",                    /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_step_formex.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 28,  /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 29,  /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 42,  /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 43,  /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 44,  /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 46,  /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 47,  /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 48,  /* lineNo */
  "pm_delta",                          /* fcnName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_delta.m"/* pathName */
};

static emlrtMCInfo emlrtMCI = { 27,    /* lineNo */
  5,                                   /* colNo */
  "error",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\lang\\error.m"/* pName */
};

static emlrtRTEInfo emlrtRTEI = { 14,  /* lineNo */
  7,                                   /* colNo */
  "pm_step_formex",                    /* fName */
  "C:\\Users\\c3162257\\Dropbox\\Cart MPC\\with hessian\\pm_step_formex.m"/* pName */
};

static emlrtRSInfo m_emlrtRSI = { 27,  /* lineNo */
  "error",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\lang\\error.m"/* pathName */
};

/* Function Declarations */
static void error(const emlrtStack *sp, const mxArray *b, emlrtMCInfo *location);

/* Function Definitions */
static void error(const emlrtStack *sp, const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(sp, 0, NULL, 1, &pArray, "error", true, location);
}

void pm_step_formex(const emlrtStack *sp, real_T xk[4], real_T uk, real_T T,
                    real_T M, const struct0_T *param, real_T dxkp1_dxk[16],
                    real_T dxkp1_duk[4], real_T *Qkp1)
{
  const mxArray *y;
  int32_T i;
  const mxArray *m3;
  static const int32_T iv5[2] = { 1, 28 };

  static const char_T varargin_1[28] = { 'M', ' ', 'm', 'u', 's', 't', ' ', 'b',
    'e', ' ', 'a', ' ', 'p', 'o', 's', 'i', 't', 'i', 'v', 'e', ' ', 'i', 'n',
    't', 'e', 'g', 'e', 'r' };

  int32_T j;
  real_T Delta;
  real_T xkp1[4];
  real_T costheta2;
  real_T B;
  real_T b_B;
  real_T dhdxk[16];
  real_T A;
  real_T c_B;
  real_T b_A;
  real_T b_y;
  real_T c_A;
  real_T c_y;
  real_T dhduk[4];
  int32_T i0;
  real_T b_dhdxk[16];
  int32_T i1;
  real_T c_dhdxk[4];
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;

  /* temp */
  if (M < 1.0) {
    st.site = &emlrtRSI;
    y = NULL;
    m3 = emlrtCreateCharArray(2, iv5);
    emlrtInitCharArrayR2013a(&st, 28, m3, &varargin_1[0]);
    emlrtAssign(&y, m3);
    b_st.site = &m_emlrtRSI;
    error(&b_st, y, &emlrtMCI);
  }

  memset(&dxkp1_dxk[0], 0, sizeof(real_T) << 4);
  for (i = 0; i < 4; i++) {
    dxkp1_dxk[i + (i << 2)] = 1.0;
    dxkp1_duk[i] = 0.0;
  }

  emlrtForLoopVectorCheckR2012b(1.0, 1.0, M, mxDOUBLE_CLASS, (int32_T)M,
    &emlrtRTEI, sp);
  j = 0;
  while (j <= (int32_T)M - 1) {
    Delta = T / M;
    st.site = &b_emlrtRSI;

    /*  Unpack States */
    /*  Unpack Inputs */
    /*  Unpack parameter structure */
    /*  Euler prediction */
    /*  Euler prediction */
    /*  ToDo: Q uncertainty */
    xkp1[0] = xk[0] + Delta * xk[2];
    xkp1[1] = xk[1] + Delta * xk[3];
    b_st.site = &c_emlrtRSI;
    costheta2 = (uk - param->b * xk[2]) + mpower(xk[3]) * param->l * param->m *
      muDoubleScalarSin(xk[1]);
    b_st.site = &c_emlrtRSI;
    B = (param->M + param->m) - param->m * mpower(muDoubleScalarCos(xk[1]));
    b_st.site = &c_emlrtRSI;
    b_B = (param->M * param->l + param->l * param->m) - param->l * param->m *
      mpower(muDoubleScalarCos(xk[1]));
    xkp1[2] = xk[2] + Delta * (costheta2 / B + muDoubleScalarCos(xk[1]) *
      (param->c * xk[3] - param->g * param->l * param->m * muDoubleScalarSin(xk
      [1])) / b_B);
    b_st.site = &d_emlrtRSI;
    B = (mpower(param->l) * mpower(param->m) - mpower(param->l) * mpower
         (param->m) * mpower(muDoubleScalarCos(xk[1]))) + param->M * mpower
      (param->l) * param->m;
    b_st.site = &d_emlrtRSI;
    costheta2 = muDoubleScalarCos(xk[1]) * ((uk - param->b * xk[2]) + mpower(xk
      [3]) * param->l * param->m * muDoubleScalarSin(xk[1]));
    b_st.site = &d_emlrtRSI;
    b_B = (param->M * param->l + param->l * param->m) - param->l * param->m *
      mpower(muDoubleScalarCos(xk[1]));
    xkp1[3] = xk[3] - Delta * ((param->c * xk[3] - param->g * param->l *
      param->m * muDoubleScalarSin(xk[1])) * (param->M + param->m) / B +
      costheta2 / b_B);

    /*  State derivities w.r.t. previous states */
    dhdxk[0] = 1.0;
    dhdxk[4] = 0.0;
    dhdxk[8] = Delta;
    dhdxk[12] = 0.0;
    dhdxk[1] = 0.0;
    dhdxk[5] = 1.0;
    dhdxk[9] = 0.0;
    dhdxk[13] = Delta;
    dhdxk[2] = 0.0;
    b_st.site = &e_emlrtRSI;
    B = (param->M * param->l + param->l * param->m) - param->l * param->m *
      mpower(muDoubleScalarCos(xk[1]));
    b_st.site = &e_emlrtRSI;
    costheta2 = mpower(xk[3]) * param->l * param->m * muDoubleScalarCos(xk[1]);
    b_st.site = &e_emlrtRSI;
    b_B = (param->M + param->m) - param->m * mpower(muDoubleScalarCos(xk[1]));
    b_st.site = &e_emlrtRSI;
    A = param->g * param->l * param->m * mpower(muDoubleScalarCos(xk[1]));
    b_st.site = &e_emlrtRSI;
    c_B = (param->M * param->l + param->l * param->m) - param->l * param->m *
      mpower(muDoubleScalarCos(xk[1]));
    b_st.site = &e_emlrtRSI;
    b_A = 2.0 * param->m * muDoubleScalarCos(xk[1]) * muDoubleScalarSin(xk[1]) *
      ((uk - param->b * xk[2]) + mpower(xk[3]) * param->l * param->m *
       muDoubleScalarSin(xk[1]));
    b_st.site = &e_emlrtRSI;
    b_y = mpower((param->M + param->m) - param->m * mpower(muDoubleScalarCos(xk
      [1])));
    b_st.site = &e_emlrtRSI;
    c_A = 2.0 * param->l * param->m * mpower(muDoubleScalarCos(xk[1])) *
      muDoubleScalarSin(xk[1]) * (param->c * xk[3] - param->g * param->l *
      param->m * muDoubleScalarSin(xk[1]));
    b_st.site = &e_emlrtRSI;
    c_y = mpower((param->M * param->l + param->l * param->m) - param->l *
                 param->m * mpower(muDoubleScalarCos(xk[1])));
    dhdxk[6] = -Delta * ((((muDoubleScalarSin(xk[1]) * (param->c * xk[3] -
      param->g * param->l * param->m * muDoubleScalarSin(xk[1])) / B - costheta2
      / b_B) + A / c_B) + b_A / b_y) + c_A / c_y);
    b_st.site = &f_emlrtRSI;
    B = (param->M + param->m) - param->m * mpower(muDoubleScalarCos(xk[1]));
    dhdxk[10] = 1.0 - Delta * param->b / B;
    b_st.site = &g_emlrtRSI;
    B = (param->M * param->l + param->l * param->m) - param->l * param->m *
      mpower(muDoubleScalarCos(xk[1]));
    b_st.site = &g_emlrtRSI;
    b_B = (param->M + param->m) - param->m * mpower(muDoubleScalarCos(xk[1]));
    dhdxk[14] = Delta * (param->c * muDoubleScalarCos(xk[1]) / B + 2.0 * xk[3] *
                         param->l * param->m * muDoubleScalarSin(xk[1]) / b_B);
    dhdxk[3] = 0.0;
    b_st.site = &h_emlrtRSI;
    costheta2 = muDoubleScalarSin(xk[1]) * ((uk - param->b * xk[2]) + mpower(xk
      [3]) * param->l * param->m * muDoubleScalarSin(xk[1]));
    b_st.site = &h_emlrtRSI;
    B = (param->M * param->l + param->l * param->m) - param->l * param->m *
      mpower(muDoubleScalarCos(xk[1]));
    b_st.site = &h_emlrtRSI;
    A = mpower(xk[3]) * param->l * param->m * mpower(muDoubleScalarCos(xk[1]));
    b_st.site = &h_emlrtRSI;
    b_B = (param->M * param->l + param->l * param->m) - param->l * param->m *
      mpower(muDoubleScalarCos(xk[1]));
    b_st.site = &h_emlrtRSI;
    c_B = (mpower(param->l) * mpower(param->m) - mpower(param->l) * mpower
           (param->m) * mpower(muDoubleScalarCos(xk[1]))) + param->M * mpower
      (param->l) * param->m;
    b_st.site = &h_emlrtRSI;
    b_A = 2.0 * param->l * param->m * mpower(muDoubleScalarCos(xk[1])) *
      muDoubleScalarSin(xk[1]) * ((uk - param->b * xk[2]) + mpower(xk[3]) *
      param->l * param->m * muDoubleScalarSin(xk[1]));
    b_st.site = &h_emlrtRSI;
    b_y = mpower((param->M * param->l + param->l * param->m) - param->l *
                 param->m * mpower(muDoubleScalarCos(xk[1])));
    b_st.site = &h_emlrtRSI;
    c_A = 2.0 * mpower(param->l) * mpower(param->m) * muDoubleScalarCos(xk[1]) *
      muDoubleScalarSin(xk[1]) * (param->c * xk[3] - param->g * param->l *
      param->m * muDoubleScalarSin(xk[1])) * (param->M + param->m);
    b_st.site = &h_emlrtRSI;
    c_y = mpower((mpower(param->l) * mpower(param->m) - mpower(param->l) *
                  mpower(param->m) * mpower(muDoubleScalarCos(xk[1]))) +
                 param->M * mpower(param->l) * param->m);
    dhdxk[7] = Delta * ((((costheta2 / B - A / b_B) + param->g * param->l *
                          param->m * muDoubleScalarCos(xk[1]) * (param->M +
      param->m) / c_B) + b_A / b_y) + c_A / c_y);
    b_st.site = &i_emlrtRSI;
    B = (param->M * param->l + param->l * param->m) - param->l * param->m *
      mpower(muDoubleScalarCos(xk[1]));
    dhdxk[11] = Delta * param->b * muDoubleScalarCos(xk[1]) / B;
    b_st.site = &j_emlrtRSI;
    B = (mpower(param->l) * mpower(param->m) - mpower(param->l) * mpower
         (param->m) * mpower(muDoubleScalarCos(xk[1]))) + param->M * mpower
      (param->l) * param->m;
    b_st.site = &j_emlrtRSI;
    b_B = (param->M * param->l + param->l * param->m) - param->l * param->m *
      mpower(muDoubleScalarCos(xk[1]));
    dhdxk[15] = 1.0 - Delta * (param->c * (param->M + param->m) / B + 2.0 * xk[3]
      * param->l * param->m * muDoubleScalarCos(xk[1]) * muDoubleScalarSin(xk[1])
      / b_B);

    /*  State derivities w.r.t. input */
    dhduk[0] = 0.0;
    dhduk[1] = 0.0;
    costheta2 = muDoubleScalarCos(xk[1]);
    costheta2 *= costheta2;
    dhduk[2] = Delta / ((param->M + param->m) - param->m * costheta2);
    dhduk[3] = -(Delta * muDoubleScalarCos(xk[1])) / ((param->M * param->l +
      param->l * param->m) - param->l * param->m * costheta2);
    for (i = 0; i < 4; i++) {
      for (i0 = 0; i0 < 4; i0++) {
        b_dhdxk[i + (i0 << 2)] = 0.0;
        for (i1 = 0; i1 < 4; i1++) {
          b_dhdxk[i + (i0 << 2)] += dhdxk[i + (i1 << 2)] * dxkp1_dxk[i1 + (i0 <<
            2)];
        }
      }
    }

    for (i = 0; i < 4; i++) {
      costheta2 = 0.0;
      for (i0 = 0; i0 < 4; i0++) {
        dxkp1_dxk[i0 + (i << 2)] = b_dhdxk[i0 + (i << 2)];
        costheta2 += dhdxk[i + (i0 << 2)] * dxkp1_duk[i0];
      }

      c_dhdxk[i] = costheta2 + dhduk[i];
    }

    for (i = 0; i < 4; i++) {
      dxkp1_duk[i] = c_dhdxk[i];
    }

    for (i = 0; i < 4; i++) {
      xk[i] = xkp1[i];
    }

    j++;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  /* ToDo Propagation of Q */
  *Qkp1 = rtNaN;
}

/* End of code generation (pm_step_formex.c) */
