/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * pm_step_formex_initialize.c
 *
 * Code generation for function 'pm_step_formex_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "pm_step_formex.h"
#include "pm_step_formex_initialize.h"
#include "_coder_pm_step_formex_mex.h"
#include "pm_step_formex_data.h"

/* Function Definitions */
void pm_step_formex_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/* End of code generation (pm_step_formex_initialize.c) */
