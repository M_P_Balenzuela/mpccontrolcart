/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * pm_step_formex.h
 *
 * Code generation for function 'pm_step_formex'
 *
 */

#ifndef PM_STEP_FORMEX_H
#define PM_STEP_FORMEX_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "pm_step_formex_types.h"

/* Function Declarations */
extern void pm_step_formex(const emlrtStack *sp, real_T xk[4], real_T uk, real_T
  T, real_T M, const struct0_T *param, real_T dxkp1_dxk[16], real_T dxkp1_duk[4],
  real_T *Qkp1);

#endif

/* End of code generation (pm_step_formex.h) */
