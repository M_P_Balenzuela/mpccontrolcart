function dV_duvect = hessfcn(uvect,~)
global x0 T Mitt PM_DELTA param sqQ sqR
M=Mitt;
%temp
nx=4;
nu=1;



N = length(uvect);

V = 0;
dV_duvect = zeros(N,1);

xk = x0;
sqrt2 = 2^0.5;
dxkp1_duk_store = nan(nx,nu,N);
dxkp1_dxk_store = nan(nx,nx,N);
xkp1_store = nan(nx,N);
for i =1:N
    uk = uvect(i);
    V = V + (sqR*uk)'*(sqR*uk); %use chol for sqR and sqQ
    [xkp1,dxkp1_dxk,dxkp1_duk,~] = pm_step(xk,uk,T,M,PM_DELTA,param);
    V = V + (sqQ*xkp1)'*(sqQ*xkp1);
    xk = xkp1;
    
    xkp1_store(:,i) = xkp1;
    dxkp1_duk_store(:,:,i) = dxkp1_duk;
    dxkp1_dxk_store(:,:,i) = dxkp1_dxk;

    % First derivitive of input
    dV_duvect(i) = sqrt2*(sqR)*uk;%dV_duvect(i) = 2*(sqR'*sqR)*uk;
    

    
end

   
for i =1:N    
    xkp1duj = dxkp1_duk_store(:,:,i)';
    
    for j = i:N
        xkp1 = xkp1_store(:,j);
        dV_duvect(i) = dV_duvect(i) + xkp1duj*(sqrt2*xkp1'*(sqQ))';
        
        if j ~= N
           xkp1duj = xkp1duj*dxkp1_dxk_store(:,:,j+1)';
        end
       
    end
end
    
dV_duvect = dV_duvect*dV_duvect';