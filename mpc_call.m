function F = mpc_call(x0e)
%#codegen
global umax N Mitt T sol param sqQ sqR

% global x0 PM_DELTA 
% x0=x0e;
%  PM_DELTA = @pm_delta;
%  PM_DELTA = @pm_delta_mex;
% fmincon...
% [V,G] = forward_prediction(uvect)
% H = forward_hessian(uvect)

lb = -umax*ones(1,N);
ub = umax*ones(1,N);
xp0 = [sol(2:end) sol(end)];
opts = optimoptions('fmincon','Display','off','SpecifyObjectiveGradient',true,'Algorithm','sqp-legacy');%,'CheckGradients', true);%;%,'HessianFcn',@hessfcn,'CheckGradients', true);  ,'Algorithm','sqp' ,'StepTolerance',10^-4 ,'Algorithm','sqp'
opts.MaxIterations = 3; 
% opts.MaxFunctionEvaluations = 20;
   tStart = tic;
% sol = fmincon(@(uvect)quadratic_mpc_cost_function(x0e,uvect,T,Mitt,PM_DELTA,param,sqQ,sqR), xp0, [], [], [], [], lb, ub, [], opts);
sol = fmincon(@(uvect)quadratic_mpc_cost_function_formex_mex(x0e,uvect,T,Mitt,param,sqQ,sqR), xp0, [], [], [], [], lb, ub, [], opts);

toc(tStart)
F=sol(1)
end