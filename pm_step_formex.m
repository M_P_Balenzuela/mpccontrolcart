function [xk,dxkp1_dxk,dxkp1_duk,Qkp1] = pm_step_formex(xk,uk,T,M,param)
%#codegen
%temp
nx =4;
nu=1;

if (M<1)
   error('M must be a positive integer');
end

dxkp1_dxk = eye(nx);
dxkp1_duk = zeros(nx,nu);

for j=1:M
    
   [xkp1,dhdxk,dhduk,~] = pm_delta(xk,uk,T/M,param); 
    dxkp1_dxk = dhdxk * dxkp1_dxk;
    dxkp1_duk = dhdxk * dxkp1_duk + dhduk;
   
   xk = xkp1;
   
end

%ToDo Propagation of Q
Qkp1 = NaN;