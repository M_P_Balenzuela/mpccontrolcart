%% Clean workspace
clc
clear all

%% Define symbolic variables
syms m M l b c g real; % Define Parameters
syms F real; % Define Inputs
syms x theta dx dtheta real; % Define States
syms Delta real % Define euler discretisation variable

% Form input vector
invect = F;
% Form state vector: z = [x;theta;dx;dtheta]
statevect= [x;theta;dx;dtheta]; % Half equations are trivial
% Form parameter vector
paramvect = [m M l b c g];

%% Form state equations
Mass=[M+m m*l*cos(theta);
    m*l*cos(theta) m*l^2];

damp=[b 0;
    0 c];

vw=-m*l*[dtheta^2;g]*sin(theta);

tau=[F;0];

%% State derivitive

xderivitive(3:4,1) = Mass^-1*(-damp*[dx;dtheta]-vw+tau);
xderivitive(1:2,1) = [dx;dtheta];

% Euler discretisation
xkpdelta = statevect + Delta * xderivitive;

%gradf = jacobian(Qij,x);
%hessf = jacobian(gradf,x)

%% Print outcome

% Packing params structure
disp('% Pack parameter structure');
for i =1:length(paramvect)
    disp(['param.' char(paramvect(i)) ' = ' char(paramvect(i)) ';']) 
end

disp(' ');
disp(' ');
disp(' ');

disp('function [h,dhdxT,dhduT,Q] = pm_delta(xk,uk,Delta,param)');
disp(' ');
disp('% Unpack States');
for i =1:length(statevect)
    disp([char(statevect(i)) ' = xk(' num2str(i) ');']);
end

disp(' ');

disp('% Unpack Inputs');
for i =1:length(invect)
    disp([char(invect(i)) ' = uk(' num2str(i) ');']);
end

disp(' ');

disp('% Unpack parameter structure');
for i =1:length(paramvect)
    disp([char(paramvect(i)) ' = param.' char(paramvect(i)) ';']);
end

disp(' ');

disp('% Euler prediction')
disp(['h = zeros(' num2str(length(statevect)) ',1);']);
for i = 1:length(statevect)
    disp(['h(' num2str(i) ',1) = ' char(xkpdelta(i)) ';']);
end
    
disp(' ');

disp('% State derivities w.r.t. previous states')
disp(['dhdxT = zeros(' num2str(length(statevect)) ');']);
for i = 1:length(statevect)
    for j = 1:length(statevect)
        disp(['dhdxT(' num2str(i) ',' num2str(j) ') = ' char(diff(xkpdelta(i),statevect(j))) ';']); % dhdx^T
    end
end

disp(' ');

disp('% State derivities w.r.t. input')
disp(['dhduT = zeros(' num2str(length(statevect)) ',' num2str(length(invect)) ');']);
for i = 1:length(statevect)
    for j = 1:length(invect)
        disp(['dhduT(' num2str(i) ',' num2str(j) ') = ' char(diff(xkpdelta(i),invect(j))) ';']); % dhdx^T
    end
end

disp(' ');

disp('% ToDo: Q uncertainty')
disp('Q = NaN;')



