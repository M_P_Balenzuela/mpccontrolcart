%tout
%state_out

% v = VideoWriter('peaks.avi');
v = VideoWriter('newfile.avi');%,'Uncompressed AVI');
T=1/24; %Update frames at 
v.FrameRate = 24;
open(v);
opengl hardware
N = size(state_out,1)
l=0.2
t=0;

tend=5;%tout(end)
figure(1);
while(t<tend)
    for i=1:N
        if tout(i) > t
            break;
        end
    end
    %considder end
    
    Delta =  (t- tout(i-1))/ (tout(i) - tout(i-1));
    
    
    x = Delta*state_out(i-1,1) + (1-Delta)*state_out(i,1);
    theta = Delta*state_out(i-1,2) + (1-Delta)*state_out(i,2);

    % Plot at instant
    clf;
    rectangle('Position',[-0.15+x,-0.05,0.3,0.05],'FaceColor','r','EdgeColor','b','LineWidth',0.3);
    hold on
    plot([x; x+l*sin(theta)],[0.0;l*cos(theta)],'k','LineWidth',1.5);
%     rectangle('Position',[1,2,5,5],'Curvature',[1,1], 'FaceColor','r')
    plot(x+l*sin(theta), l*cos(theta), '.k', 'MarkerSize',40)
    
    
    axis([-1-0.3 1-0.5 -1+0.5 1+0.3])
    % Update time

    pause(T);
    t=t+T;

    frame = getframe(gcf);
    writeVideo(v,frame);
end

close(v);
