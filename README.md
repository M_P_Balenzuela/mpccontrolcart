# README #

Welcome to my repo for nonlinear Model Predictive Control (MPC) of a pendulum on a cart.

This code accompanies the simulation provided by the YouTube link:
https://www.youtube.com/watch?v=DM4ItLabFs0&ab_channel=MarkBalenzuela

The provided code was written to demonstrate my understanding of MPC control as a side project, and is not a detailed simulation as state estimation has not yet been implemented.
Additionally, further improvements need to be made on the efficiency of the code to bring the solve time down, as the control rate is faster than the solve rate.
This would likely require moving away from MATLAB's built in solvers and write my own solver, specifically optimised for this problem (possibly using Quasi-Neuton methods?).

I highly recommend that those wishing to learn more about this project or MPC control read the following (freely available) paper:
Mills, A., Wills, A. and Ninness, B., 2009, June. 
Nonlinear model predictive control of an inverted pendulum. 
In 2009 American control conference (pp. 2335-2340). IEEE.

Which is available for download at:
https://ieeexplore.ieee.org/abstract/document/5160391?casa_token=Fnbx5QiditcAAAAA:t10E5kTs_0kp_J0J6I90usaUgwZGJGoeTErrY9OQihhzIxYD8ss1sh4vCSOFG764AkAyJ6Rmfw

This paper appears to accompany their YouTube demonstration video available here:
https://www.youtube.com/watch?v=b9eDwB9bSqE&ab_channel=sigpromu

### What is this repository for? ###

This code is provided to assist those beginning to learn MPC control, and simply need a basic working example to understand the concept of MPC control.
I wrote this code at a time when I had convenient access to experts in MPC and could ask questions face-to-face, and it was never intended to become a complete a detailed simulation, but rather a small simulation designed to test my understanding.

This code was released upon request of commenters on the YouTube video. It is not necisarily up to the standard I would normally release code, as it was originally written for just myself as a demo.

### How do I get set up? ###

This code requires a MATLAB installation and was most recently tested on version (2018a).
Simulink is also required to be installed.
MATLAB's CodeGen ability is also used (for faster solve time), and my installation is setup with MEX configured to use 'MinGW64 Compiler (C)' for C language compilation.

Run main.m to begin the simulation. After it has completed, the simulation will be graphically replayed and saved to newfile.avi for lack of a better name.


### Who do I talk to? ###

This code is not maintained and is provided 'as is'. I am not responsible for its use, or any consequences thereof.