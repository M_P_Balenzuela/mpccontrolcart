function [V, dV_duvect] = quadratic_mpc_cost_function_formex(x0,uvect,T,M,param,sqQ,sqR)
%#codegen
% tic 
%codegen -report quadratic_mpc_cost_function_formex.m -args {x0,uvect,T,M,param,sqQ,sqR}
%temp
nx=4;
nu=1;



N = length(uvect);

V = 0;
dV_duvect = zeros(N,1);

xk = x0;

dxkp1_duk_store = nan(nx,nu,N);
dxkp1_dxk_store = nan(nx,nx,N);
xkp1_store = nan(nx,N);
for i =1:N
    uk = uvect(i);
    V = V + (sqR*uk)'*(sqR*uk); %use chol for sqR and sqQ
%     [xkp1,dxkp1_dxk,dxkp1_duk,~] = pm_step(xk,uk,T,M,PM_DELTA,param);
    [xkp1,dxkp1_dxk,dxkp1_duk,~] = pm_step_formex(xk,uk,T,M,param);
    V = V + (sqQ*xkp1)'*(sqQ*xkp1);
    xk = xkp1;
    
    xkp1_store(:,i) = xkp1;
    dxkp1_duk_store(:,:,i) = dxkp1_duk;
    dxkp1_dxk_store(:,:,i) = dxkp1_dxk;

    % First derivitive of input
    dV_duvect(i) = 2*(sqR'*sqR)*uk;
    

    
end
t2Q = (sqQ'*sqQ)*2;
   
for i =1:N    
    xkp1duj = dxkp1_duk_store(:,:,i)';
    
    for j = i:N
        xkp1 = xkp1_store(:,j);
%         dV_duvect(i) = dV_duvect(i) + xkp1duj*(2*xkp1'*(sqQ'*sqQ))';
%             dV_duvect(i) = dV_duvect(i) + xkp1duj*(sqQ'*sqQ)*2*xkp1;
            t3  =xkp1duj*t2Q;
            dV_duvect(i) = dV_duvect(i) + t3*xkp1;
        if j ~= N
            t4=dxkp1_dxk_store(:,:,j+1)';
           xkp1duj = xkp1duj*t4;
        end
       
    end
end
    
dV_duvect = dV_duvect';
% toc