function [h,dhdxT,dhduT,Q] = pm_delta(xk,uk,Delta,param)
%#codegen
 
% Unpack States
x = xk(1);
theta = xk(2);
dx = xk(3);
dtheta = xk(4);
 
% Unpack Inputs
F = uk(1);
 
% Unpack parameter structure
m = param.m;
M = param.M;
l = param.l;
b = param.b;
c = param.c;
g = param.g;
 
sintheta = sin(theta);
ml = m*l;
Ml = M*l;
costheta2 = cos(theta);
costheta2=costheta2*costheta2;
% Euler prediction
% Euler prediction
% ToDo: Q uncertainty
Q = 0;
h = zeros(4,1);
h(1,1) = x + Delta*dx;
h(2,1) = theta + Delta*dtheta;
h(3,1) = dx + Delta*((F - b*dx + dtheta^2*l*m*sintheta)/(M + m - m*cos(theta)^2) + (cos(theta)*(c*dtheta - g*ml*sintheta))/(Ml + ml - ml*costheta2));
h(4,1) = dtheta - Delta*(((c*dtheta - g*ml*sintheta)*(M + m))/(ml^2 - ml^2*cos(theta)^2 + M*l^2*m) + (cos(theta)*(F - b*dx + dtheta^2*ml*sintheta))/(Ml + ml - ml*cos(theta)^2));
 


% State derivities w.r.t. previous states
dhdxT = zeros(4);
dhdxT(1,1) = 1;
dhdxT(1,2) = 0;
dhdxT(1,3) = Delta;
dhdxT(1,4) = 0;
dhdxT(2,1) = 0;
dhdxT(2,2) = 1;
dhdxT(2,3) = 0;
dhdxT(2,4) = Delta;
dhdxT(3,1) = 0;
dhdxT(3,2) = -Delta*((sintheta*(c*dtheta - g*l*m*sintheta))/(Ml + ml - l*m*cos(theta)^2) - (dtheta^2*l*m*cos(theta))/(M + m - m*cos(theta)^2) + (g*ml*costheta2)/(Ml + ml - ml*costheta2) + (2*m*cos(theta)*sintheta*(F - b*dx + dtheta^2*l*m*sintheta))/(M + m - m*cos(theta)^2)^2 + (2*l*m*cos(theta)^2*sintheta*(c*dtheta - g*l*m*sintheta))/(M*l + l*m - l*m*costheta2)^2);
dhdxT(3,3) = 1 - (Delta*b)/(M + m - m*costheta2);
dhdxT(3,4) = Delta*((c*cos(theta))/(M*l + ml - ml*costheta2) + (2*dtheta*ml*sintheta)/(M + m - m*costheta2));
dhdxT(4,1) = 0;
dhdxT(4,2) = Delta*((sintheta*(F - b*dx + dtheta^2*ml*sintheta))/(Ml + ml - ml*costheta2) - (dtheta^2*ml*costheta2)/(Ml + ml - ml*costheta2) + (g*ml*cos(theta)*(M + m))/(ml^2 - ml^2*costheta2 + M*l^2*m) + (2*l*m*cos(theta)^2*sintheta*(F - b*dx + dtheta^2*ml*sintheta))/(M*l + l*m - ml*cos(theta)^2)^2 + (2*l^2*m^2*cos(theta)*sintheta*(c*dtheta - g*l*m*sintheta)*(M + m))/(l^2*m^2 - ml^2*costheta2 + M*l^2*m)^2);
dhdxT(4,3) = (Delta*b*cos(theta))/(Ml + ml - ml*cos(theta)^2);
dhdxT(4,4) = 1 - Delta*((c*(M + m))/(ml^2 - ml^2*cos(theta)^2 + M*l^2*m) + (2*dtheta*ml*cos(theta)*sintheta)/(Ml + ml - l*m*costheta2));
 
% State derivities w.r.t. input
dhduT = zeros(4,1);
dhduT(1,1) = 0;
dhduT(2,1) = 0;

dhduT(3,1) = Delta/(M + m - m*costheta2);
dhduT(4,1) = -(Delta*cos(theta))/(Ml + ml - ml*costheta2);
 


