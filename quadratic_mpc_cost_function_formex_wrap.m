function [V, dV_duvect] = quadratic_mpc_cost_function_formex_wrap(x0,uvect,T,M,param,sqQ,sqR)

% param.mnade=1;
tic
[V, dV_duvect]=quadratic_mpc_cost_function_formex_mex(x0,uvect,T,M,param,sqQ,sqR);
toc