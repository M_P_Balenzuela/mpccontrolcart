%% Clean workspace
close all;
clear all;
clc;
%global umax x0 b sqQ sqR N M T l  xt ustep sol
global sol param Mitt T N umax sqQ sqR
%% Load parameters

xt=0;
ustep=0;
dx0 = [0;0]; %[initial cart velocity;initial pendulum ang. vel]
x0 = [0;pi]; % [initial position; initial angle (rad)]



umax = 15.5; % Maximum control input []
% Pmax = 0.7; % Maximum Cart position [m]
% SQPmaxItter = 4; % Maximum SQP itterations
N = 60; % Predicition horizon
T = 0.025; % Sample time [s]
Mitt = 100; % Number of prediction sub-steps

sol = ones(1,N);

% SQP cost params
q1 = 1;
q2 = 10;
q3 = 10^-4;
q4 = q3;
r = 0.1;

sqQ = chol(diag([q1 q2 q3 q4]));
sqR=sqrt(r);

% Plant params
l = 0.3; % CoG location of pendulum [m]
m = 0.15; % Pendulum mass [kg]
M = 0.55; % Cart mass [kg]
b = 0.05; % Cart friction [Ns/m]
c = 0.01; % Rotational friction [Nms/Rad]
g=9.8;

param.m = m;
param.M = M;
param.l = l;
param.b = b;
param.c = c;
param.g = g;

% Sensor paramsmp
cartDisplacementQuantisation = 0.005; % Cart position Quantisation [m]
pendulumAngleQuantisation = deg2rad(0.1); % Pendulum angle quantisation [Rad]

% Simulation param
tsim = 5; % Simulation time [s]



%% Run simulation
sim('simu');


plotstuff;