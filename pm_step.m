function [xkp1,dxkp1_dxk,dxkp1_duk,Qkp1] = pm_step(xk,uk,T,M,PM_DELTA,param)
%#codegen
%temp
nx =4;
nu=1;

if (M<1)
   error('M must be a positive integrer');
end

dxkp1_dxk = 1;
dxkp1_duk = zeros(nx,nu);

for j=1:M
    
   [xkp1,dhdxk,dhduk,~] = PM_DELTA(xk,uk,T/M,param); 
    dxkp1_dxk = dhdxk * dxkp1_dxk;
    dxkp1_duk = dhdxk * dxkp1_duk + dhduk;
   
   xk = xkp1;
   
end

%ToDo Propagation of Q
Qkp1 = NaN;